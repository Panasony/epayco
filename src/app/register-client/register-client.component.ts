import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Client } from '../insterfaces/client';
import { ClientService } from '../services/client.service'
declare var $: any;

@Component({
  selector: 'app-register-client',
  templateUrl: './register-client.component.html',
  styleUrls: ['./register-client.component.css']
})
export class RegisterClientComponent implements OnInit {

  client: Client = {
    name: '',
    document:'',
    email: '',
    telephone: null
  }

  constructor(
    private router: Router,
    private clientService: ClientService,
  ) { }

  ngOnInit(): void {
  }

  listClients() {
    this.router.navigate(['/client']);
  }

  addClient(forma: NgForm) {
    this.clientService.addClient(this.client).subscribe( (res) => {
      console.log(res);
      this.showNotification('bottom','center');
      this.listClients();
    });
  }

  showNotification(from, align){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."

    },{
        type: type[color],
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: 
        '<div class="col-md-6">'+
        '<div class="alert alert-success">'+
        '<button mat-button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<i class="material-icons">close</i>'+
        '</button>'+
        '<span>'+
            '<b> Success - </b> Successfully registered customer </span>'+
        '</div>'+
        '</div>'
    });
}

}
