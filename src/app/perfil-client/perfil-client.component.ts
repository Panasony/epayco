import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Client } from 'app/insterfaces/client';
import { ClientService } from '../services/client.service'
import { NgForm } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-perfil-client',
  templateUrl: './perfil-client.component.html',
  styleUrls: ['./perfil-client.component.css']
})
export class PerfilClientComponent implements OnInit {

  @ViewChild('exampleModalCenter') exampleModalCenter;
  @ViewChild('exampleModalCenterPay') exampleModalCenterPay;

  public client: Client;
  public id: string;
  public checkBalance: any;
  public recharge: object = {
    amount: null
  };

  public payClient: object = {
    amount: null
  };

  constructor( 
    private clientService: ClientService,
    private activatedRoute: ActivatedRoute
  ) { 
      this.activatedRoute.params.subscribe(paramsId => {
          this.id = paramsId.id;
      });
      this.getInfoClient(this.id);
   }

  ngOnInit(): void {
  }

  getInfoClient(id: string) {
    this.clientService.getAllClientById(id).subscribe(async (res) => {
      this.client = res.user;
      console.log(this.client, 'Datos del cliente');
    })
  }

  rechargeWallets() {
    this.clientService.rechargeClient(this.client, this.recharge).subscribe((res) => {
      console.log(res, 'Recargado');
      this.showNotification('bottom','center');
      this.exampleModalCenter.nativeElement.click();
    }, err => {
      console.log(err);
      this.showNotificationDangerWallet('bottom','center');
    })
  }

  payAmount() {
    this.clientService.payClient(this.id, this.payClient).subscribe((res) => {
      console.log(res, ' Se hecho el pago');
      this.showNotificationPay('bottom','center');
      this.exampleModalCenterPay.nativeElement.click();
    }, err => {
      this.showNotificationDanger('bottom','center');
      console.log(err, 'Hubo un error');
    })
  }

  statusClient() {
    this.clientService.statusClient(this.client).subscribe( (res) => {
      this.checkBalance = res.amount;
      console.log( this.checkBalance, 'Saldo del cliente');
    })
  }

  showNotification(from, align){  
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."

    },{
        type: type[color],
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: 
        '<div class="col-md-6">'+
        '<div class="alert alert-success">'+
        '<button mat-button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<i class="material-icons">close</i>'+
        '</button>'+
        '<span>'+
            '<b> Successful - </b> Your transaction was successful </span>'+
        '</div>'+
        '</div>'
    });
  }

    showNotificationPay(from, align){  
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."

    },{
        type: type[color],
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: 
        '<div class="col-md-6">'+
        '<div class="alert alert-success">'+
        '<button mat-button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<i class="material-icons">close</i>'+
        '</button>'+
        '<span>'+
            '<b> Successful - </b> An email has been sent to verify your payment </span>'+
        '</div>'+
        '</div>'
    });
  }


  showNotificationDanger(from, align){  
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."

    },{
        type: type[color],
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: 
        '<div class="col-md-6">'+
        '<div class="alert alert-danger">'+
        '<button mat-button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<i class="material-icons">close</i>'+
        '</button>'+
        '<span>'+
            '<b> Error - </b> Cannot be discounted, insufficient wallet amount! </span>'+
        '</div>'+
        '</div>'
    });
  }

  showNotificationDangerWallet(from, align){  
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Welcome to <b>Material Dashboard</b> - a beautiful freebie for every web developer."

    },{
        type: type[color],
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: 
        '<div class="col-md-6">'+
        '<div class="alert alert-danger">'+
        '<button mat-button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<i class="material-icons">close</i>'+
        '</button>'+
        '<span>'+
            '<b> Error - </b> An error!, amount must be greater than zero! </span>'+
        '</div>'+
        '</div>'
    });
  }

}
