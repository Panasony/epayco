import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'app/insterfaces/client';
import { ClientService } from '../services/client.service'

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  client: any = [];
  message:string;

  constructor(
    private router: Router,
    private clientService: ClientService
  ) { }

  ngOnInit(): void {
    this.getClient();
  }

  getClient() {
    this.clientService.getAllClient().subscribe((res) => {
      console.log(res, 'Todos los clientes registrados');
      this.client = res.users;
      this.message =res.message;
    });
  }

  addClient() {
    this.router.navigate(['/client-register']);
  }

  perfilClient(id: string) {
    console.log(id, 'id');
    this.router.navigate(['/client-perfil', id]);
  }

}
