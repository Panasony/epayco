export interface Client {
    document:string;
    name: string;
    email: string;
    telephone: number;
}
