import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Client } from '../insterfaces/client'
import { environment } from '../../environments/environment'
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  apiURL = environment.apiUrl;

  constructor( private http:HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    console.log(error);
    
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  getAllClient(): Observable<any> {
    return this.http.get(this.apiURL+'users').pipe(
      catchError(this.handleError)
    );
  }

  getAllClientById(id: string): Observable<any> {
    return this.http.get(this.apiURL+'users/'+id).pipe(
      catchError(this.handleError)
    );
  }

  addClient(client: Client): Observable<any> {
    return this.http.post( this.apiURL +'users', client).pipe(
      catchError(this.handleError)
    );
  }

  rechargeClient(client: Client, recharge: object): Observable<any> {
    console.log(client, ' clientes', recharge , 'recarga');
    return this.http.post( this.apiURL +'wallets/recharge/'+ client.document+'/phone/'+client.telephone, recharge ).pipe(
      catchError(this.handleError)
    );
  }

  payClient(id: string, pay: object) {
    console.log(id, 'id que esta llegando', pay, 'el pago que esta por realizar');
    return this.http.post(this.apiURL+'wallets/generate-token/'+id, pay).pipe(
      catchError(this.handleError)
    );
  }

  statusClient(client: Client): Observable<any> {
    return this.http.get( this.apiURL +'wallets/status/'+ client.document+'/phone/'+client.telephone).pipe(
      catchError(this.handleError)
    );
  }


}

