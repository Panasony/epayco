# Garage APP
ePayco APP is an interface designed for a system that must be able to register a client, load money to the wallet, make
a purchase with a confirmation code and check the wallet balance.

## Table of Contents

* [Technical Support or Questions](#technical-support-or-questions)
* [Documentation](#docs)
* [Licensing](#licensing)

## Terminal Commands

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Make sure you have installed Angular CLI already. If not, please install.
5. Run in terminal: ```npm install```
6. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

To get more help on the Angular CLI use ng help or go check out the Angular CLI README.

## Technical Support or Questions

If you have questions or need help integrating the app please[ contact me](https://github.com/Panasony19) instead of opening an issue.

## Documentation

To see the documentation endpoint, go to `http://localhost:3000/`.


## Licensing

- Copyright 2020 Ansony Alvarado (https://github.com/Panasony19)

- Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-angular2/blob/master/LICENSE.md)
